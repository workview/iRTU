# 数据流

## 操作步骤

此次以mqtt向多主题发送数据为例：

![数据流1](img/数据流1.png)

mqtt多主题格式：

topic1;qos;topic2;qos...(每个参数之间必须加；最后一个参数后不需要加；)

1             2      3         4（每个参数对应一位，依次递增）

测试代码如下：

```
function ()  ##dtu固定格式（function()  代码 end）

  local str=...##接收的字符串

  if str:sub(1,5)=="error" then  ##自定义格式

    return str:sub(6,-1),3  ##返回值

  end



  return str,1  ## 返回值

end
```

### 修改参数

![数据流2](img/数据流2.png)

### 日志打印

重启模块后：

![数据流3](img/数据流3.png)

![数据流4](img/数据流4.png)

![数据流5](img/数据流5.png)

![数据流6](img/数据流6.png)
